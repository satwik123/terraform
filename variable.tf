variable "name" {default  = "web-server2"}
variable "environment" {default = "production"}
variable "machine_type" {default = "n1-standard-1"}
variable "machine_type_dev" {default = "n1-standard-4"}
variable "image" {default = "ubuntu-os-cloud/ubuntu-1604-lts"}
variable "image1" {default = "centos-cloud/centos-7"}
variable "disk-name" {default = "abcd"}
variable "name_count" {default = ["server1" , "server2" , "server3"]}

variable "zone" {default = "us-central1-c"}

variable service_account_email {
  description = "The email of the service account for the instance template."
  default     = "default"
}

variable service_account_scopes {
  description = "List of scopes for the instance template service account"
  type        = list

  default = [
    "https://www.googleapis.com/auth/compute",
    "https://www.googleapis.com/auth/logging.write",
    "https://www.googleapis.com/auth/monitoring.write",
    "https://www.googleapis.com/auth/devstorage.full_control",
  ]
}
