terraform {
  backend "gcs" {
    bucket  = "terraform-state_1234"
    prefix  = "terraform/state"
  }
}