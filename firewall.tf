resource "google_compute_firewall" "anything" {
    name = "http-allow"
    network = "default"

    allow{
        protocol = "tcp"
        ports = ["80"]
    }
    target_tags = ["allow-http"]
}

resource "google_compute_firewall" "another-one" {
    name = "https-allow"
    network = "default"

    allow{
        protocol = "tcp"
        ports = ["443"]
    }
    target_tags = ["allow-https"]
}
