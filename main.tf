/*data "terraform_remote_state" "foo" {
  backend = "gcs"
  config = {
    bucket  = "terraform-state_1234"
    prefix  = "prod"
  }
}

resource "template_file" "bar" {
  template = "${greeting}"

  vars {
    greeting = "${data.terraform_remote_state.foo.greeting}"
  }
}*/

resource "google_compute_instance" "default" {
  
  machine_type = var.environment == "production" ? var.machine_type : var.machine_type_dev
  name = "${var.name}"
  zone = "${var.zone}"
  can_ip_forward = "false"
  description = "Test virtual machine deployed through terraform"
  tags = ["allow-http" , "allow-https"]       #For Firewall

  boot_disk {
      initialize_params {
          image = var.environment == "production" ? var.image : var.image1
          size = "20"
      }

  }
  #count = "${length(var.service_account_scopes)}"

  service_account {
    email  = "${var.service_account_email}"
    scopes = var.service_account_scopes
 #   scopes = "${element(var.service_account_scopes , count.index)}"
  }

  
  labels = {

    name = "server-2"
    machine_type = "${var.environment == "production" ? var.machine_type : var.machine_type_dev}"

  } 

  metadata = {
    size="30"
    foo="bar"
  }

  metadata_startup_script = "apt-get update ; apt-get install -y nginx"

  network_interface {
      network = "default"
  }

  /*ervice_account {

      scopes = ["userinfo-email" , "compute-ro" , "storage-ro"]
  }*/
}

resource "google_compute_disk" "default" {
  name = var.disk-name
  type = "pd-ssd"
  zone = var.zone
  size = 10
}


resource "google_compute_attached_disk" "default" {
  disk = "${google_compute_disk.default.self_link}"
  instance = "${google_compute_instance.default.self_link}"
}

/*output "machine_type" {
  value = google_compute_instance.default.*.machine_type
}

output "Name" {
  value = google_compute_instance.default.*.name
}

output "Instance_id" {
  value = join(",",google_compute_instance.default.*.id)
}*/
